import java.util.Scanner;

public class SqrtX {

    public static int sqtr(int x){
        int first = 1, last = x;
        while (first <= last) {
            int mid = first + (last - first) / 2;
            if (mid == x / mid) {
                return mid;
            } else if (mid > x / mid) {
                last = mid - 1;
            } else {
                first = mid + 1;
            }
        }
        return last;
    }
    public static void main(String[] args) throws Exception {
        Scanner kb = new Scanner(System.in);
        System.out.print("X = ");
        int x = kb.nextInt();
        int result = sqtr(x);
        System.out.println(result);
    }
}
